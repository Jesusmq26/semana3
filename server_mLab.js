require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const request_json = require('request-json');
const app = express();
var  port = process.env.PORT || 3000;
const URL_BASE = '/techu/v2/';
const URL_mLab = 'https://api.mlab.com/api/1/databases/techu10db/collections/'
const usersFile = require('./users.json');
const apikey_mlab = 'apiKey=' + process.env.API_KEY_MLAB;

app.listen(port,function(){
	console.log("NodeJS escuchando en el puerto " + port);
});

app.use(body_parser.json()); // support json encoded bodies

app.get(URL_BASE + 'users',
  function(req, res) {
    const httpClient = request_json.createClient(URL_mLab);
    console.log("Cliente HTTP mLab creado.");
    const fieldParam = 'f={"_id":0}&';
	console.log('user?' + fieldParam+ apikey_mlab);
    httpClient.get('user?' + fieldParam+ apikey_mlab,
      function(error, res_mlab, body) {
        console.log('Error: ' + error);
        console.log('Respuesta MLab: ' + res_mlab);
        console.log('Body: ' + body);
        var response = {};
        if(error) {
            response = {"msg" : "Error al recuperar users de mLab."}
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {"msg" : "Usuario no encontrado."};
            res.status(404);
          }
        }
        res.send(response);
      }); 
});

app.get(URL_BASE + 'users/:id',
	function(req, res){
		console.log('GET /api/v2/users/:id');		
		console.log(req.params.id);
		let id = req.params.id;
		let queryString = 'q={"id_user":' + id + '}&';
		const queryStrField = 'f={"_id":0}&';
		const httpClient = request_json.createClient(URL_mLab);
		httpClient.get('user?' + queryString + queryStrField + apikey_mlab, (error, res_mlab, body) => {
				let response = {};
				if(error){
					response = {'msg' : 'Error en la peticion a mLab'};
					res.status(500);
				}else{
					if(body.length > 0) {
						response = body;
					 } else {
						response = {"msg" : "Usuario no encontrado."};
						res.status(404);
					 }
				}
				res.send(response);
	    });
});

app.get(URL_BASE +  'users/:id/accounts',
	(req, res) => {
		console.log(req.params.id);
		let id = req.params.id;
		let fieldString = 'f={"_id":0}&';
		let queryString = 'q={"user_id": ${id} }&';
		const httpClient = request_json.createClient(URL_mLab);
		httpClient.get('account?' + queryString + fieldString + apikey_mlab, (error, res_mlab, body) => {
				let respuesta = {};
				if (error) {
					respuesta = { 'msg': 'Error en la peticion a mLab' };
					res.status(500);
				 } else {
					if (body.length > 0) {
					  respuesta = body[0];
					} else {
					  respuesta = { 'msg': 'Cuentas no encontradas' };
					  res.status(404);					  
					}
				 }
				 res.send(respuesta);
			});
	});

//POST user
app.post(URL_BASE + "users",
  function (req, res) {
    var clienteMlab = request_json.createClient(URL_mLab);
    let queryString = `s={"id_user":-1}&l=1&`;
    clienteMlab.get('user?' + queryString + apikey_mlab,
      function (error, respuestaMLab, body) {
        newID = req.body.id_user + 1;
        console.log("newID:" + newID);
        var newUser = {
          "id_user": newID + 1,
          "first_name": req.body.first_name,
          "last_name": req.body.last_name,
          "email": req.body.email,
          "password": req.body.password
        };
        clienteMlab.post(URL_mLab + "user?" + apikey_mlab, newUser,
          function (error, respuestaMLab, body) {
            res.send(body);
          });
      });
  });

app.put(URL_BASE + 'users/:id',
  function(req, res) {
    let id = req.params.id;
    let queryString = 'q={"id_user":' + id + '}&';
    var http_client = request_json.createClient(URL_mLab);
    http_client.get('user?' + queryString + apikey_mlab,
     function (err, resMLab, body) {
        var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
        http_client.put(URL_mLab + 'user?' + queryString + apikey_mlab, JSON.parse(cambio),
        function (error, respuestaMLab, body) {
            console.log("body:" + body); // body.n == 1 si se pudo hacer el update
            res.send(body);
            //(body.n == 1) ? res.status(200).send(body) :  res.status(404).send(body);
          });
      });
  });

// Method Post Login
app.post(URL_BASE + 'login',
  function(request, response) {
    let email = request.body.email;
    let pass = request.body.password;
	let queryString = 'q={"email":"' + email + '","password":"' + pass + '"}&';
    let limFilter = 'l=1&';
    let clienteMlab = request_json.createClient(URL_mLab);
	clienteMlab.get('user?' + queryString + limFilter + apikey_mlab,
      function (error, respuestaMLab, body) {
        if (!error) {
          if (body.length == 1) { // Existe un usuario que cumple 'queryString'
            let login = '{"$set":{"logged":true}}';
            clienteMlab.put('user?q={"id_user": ' + body[0].id_user + '}&' + apikey_mlab, JSON.parse(login),
              //clienteMlab.put('user/' + body[0]._id.$oid + '?' + apikeyMLab, JSON.parse(login),
              function (errPut, resPut, bodyPut) {
                response.send({ 'msg': 'Login correcto', 'user': body[0].email, 'userid': body[0].id_user, 'name': body[0].first_name });
              });
          }
          else {
            response.status(404).send({ "msg": "Usuario no válido." });
          }
        } else {
          response.status(500).send({ "msg": "Error en petición a mLab." });
        }
      });   
});


app.post(URL_BASE + 'logout',
     function(request, response) {
		var email = request.body.email;
		var queryString = 'q={"email":"' + email + '","logged":true}&';
		console.log(queryString);
		var clienteMlab = request_json.createClient(URL_mLab);
		clienteMlab.get('user?' + queryString + apikey_mlab,
		  function (error, respuestaMLab, body) {
			var respuesta = body[0]; // Asegurar único usuario
			if (!error) {
			  if (respuesta != undefined) { // Existe un usuario que cumple 'queryString'
				let logout = '{"$unset":{"logged":true}}';
				clienteMlab.put('user?q={"id_user": ' + respuesta.id_user + '}&' + apikey_mlab, JSON.parse(logout),
				  //clienteMlab.put('user/' + respuesta._id.$oid + '?' + apikeyMLab, JSON.parse(logout),
				  function (errPut, resPut, bodyPut) {
					response.send({ 'msg': 'Logout correcto', 'user': respuesta.email });
					// If bodyPut.n == 1, put de mLab correcto
				  });
			  } else {
				response.status(404).send({ "msg": "Logout failed!" });
			  }
			} else {
			  response.status(500).send({ "msg": "Error en petición a mLab." });
			}
	});
});

