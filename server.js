require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const app = express();
var  port = process.env.PORT || 3000;
const URL_BASE = '/techu/v1/';
const usersFile = require('./users.json');

app.listen(port,function(){
	console.log("NodeJS escuchando en el puerto " + port);
});

app.use(body_parser.json());

//Operacion GET
app.get(URL_BASE + 'users',
	function(request, response){
		//response.status(200);
		response.status(200).send(usersFile); // La respuesta {send} al final
});

// Peticion GET a un unico usuarios mediante id (Instancia)
app.get(URL_BASE + 'users/:id',
	function(request, response){
		console.log('request.params.id');		
		let pos = request.params.id - 1;
		let respuesta = (usersFile[post] == undefined) ? {"msg":"Usuario no existente"} : usersFile[pos];
		response.send(usersFile[pos]);
});

//Petición POST a users
app.post(URL_BASE + 'users',
	function(req, res){
		console.log('POST a users');
		let tam = usersFile.length; 
		let new_user = {
			"ID": tam+1,
			"firsr_name": req.body.first_name,
			"last_name": req.body.last_name,	
			"email": req.body.email,
			"password": req.body.password
		}
		console.log(new_user);
		usersFile.push(new_user);
		res.status(201).send({"msg":"Usuario creado correctamente"});
});


//Peticion PUT - modificar user
app.put(URL_BASE + 'users/:id',
	function(req, res){
		const id = req.params.id;	
		let userIndex = usersFile.findIndex(x=>{return x.id_user == id})
		if (userIndex == -1) {
			res.status(404).send({"msg": "No se encontró usuario con id: " + id });
		}else{
		
			let new_user = {
				"id_user": id,
				"firsr_name": req.body.first_name,
				"last_name": req.body.last_name,	
				"email": req.body.email,
				"password": req.body.password
			}
			
			usersFile[userIndex]=new_user;
			res.status(202).send({"msg":"Usuario con id:" + req.params.id + " modificado correctamente."});		
		}
});


//Peticion DELETE - eliminar user
app.delete(URL_BASE + 'user/:id',
	function(req, res){
		console.log('request.params.id');		
		let pos = usersFile.findIndex(v => v.id_user == req.params.id);
		if(pos == -1){
			res.status(404).send({"msg" : "Usuario con id: "+ req.params.id + " no existe."})
		}
		usersFile.splice(req.params.id ,1);
		res.status(200).send({"msg":"Usuario con id:" + req.params.id + " eliminado correctamente."});		
});

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("POST /techu/v1/login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.status(201).send({"msg" : "Login correctos.", "idUsuario" : us.id_user, "logged" : "true"});
        } else {
          console.log("Login incorrecto.");
          response.status(404).send({"msg" : "Login incorrecto."});
        }
      }
    }
});

// LOGOUT - users.json
app.post(URL_BASE + 'logout',
     function(request, response) {
       console.log("POST /techu/v1/logout");
       var userId = request.body.id;
       for(us of usersFile) {
         if(us.id_user == userId) {
           console.log('entro for Logout email');
		   console.log(us.logged);
           if(us.logged) {
             delete us.logged; // borramos propiedad 'logged'
			 writeUserDataToFile(usersFile);
             console.log("Logout correcto!");
             response.status(201).send({"msg" : "Logout correcto.", "idUsuario" : us.id_user});
			 return;
          } else {
            console.log("Logout incorrecto.");
            response.status(404).send({"msg" : "Logout incorrecto."});
          }
        }	
      }
	  console.log("Logout incorrecto.");
	response.send({"msg" : "Logout incorrecto."});
});


function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'users.json'.");
     }
   });
 }
 
 